package com.enzo.gedima_gination.models;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          Vote
 * Description:    Object correspondant a un vote
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class Vote {
    //Variables
    String codeVotant;
    int idParticipation;
    int vote;

    //Constructor
    public Vote(String codeVotant, int idParticipation, int vote) {
        this.codeVotant = codeVotant;
        this.idParticipation = idParticipation;
        this.vote = vote;
    }

    //Getters
    public String getCodeVotant() { return codeVotant; }

    public int getIdParticipation() { return idParticipation; }

    public int getVote() { return vote; }

    //Setters
    public void setCodeVotant(String codeVotant) { this.codeVotant = codeVotant; }

    public void setIdParticipation(int idParticipation) { this.idParticipation = idParticipation; }

    public void setVote(int vote) { this.vote = vote; }
}
