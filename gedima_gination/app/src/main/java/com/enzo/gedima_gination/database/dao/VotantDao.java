package com.enzo.gedima_gination.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.enzo.gedima_gination.database.DatabaseHelper;
import com.enzo.gedima_gination.models.Data;
import com.enzo.gedima_gination.models.Votant;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          VotantDao
 * Description:    Class gérant les requetes pour la table Votant
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class VotantDao {
    //Variables
    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;

    //Constructor
    public VotantDao(Context context){
        this.databaseHelper = new DatabaseHelper(context);
        this.database = databaseHelper.getWritableDatabase();
    }

    /**
     * Methodes permetant de sélection un votant selon son id
     * @param id id du votant que l'on veut sélection
     * @return le votant si il a été trouvé ou null dans le cas contraire
     */
    public Votant selectionnerVotant(String id) {
        Cursor cursor = database.rawQuery("SELECT * FROM votant WHERE code_votant = ?",
                new String[]{id});
        cursor.moveToFirst();

        Votant votant = null;

        if(cursor.getCount() > 0){
            votant = new Votant(cursor.getString(0),cursor.getString(1), cursor.getString(2),
                    cursor.getString(3),cursor.getString(4), cursor.getString(5));
        }

        cursor.close();
        return votant;
    }

    /**
     * Methodes permettant d'ajouter un votant
     * @param votant votant que l'on souhaite ajouter
     */
    public void ajouterVotant(Votant votant){
        ContentValues values = new ContentValues();

        values.put("code_votant", votant.getCodeVotant());
        values.put("email", votant.getEmail());
        values.put("nom", votant.getNom());
        values.put("prenom", votant.getPrenom());
        values.put("consentement", votant.getConsentement());
        values.put("date_consentement", votant.getDateConsentement());

        database.insert("votant", null, values);
    }
}
