package com.enzo.gedima_gination.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.enzo.gedima_gination.utils.ApiController;
import com.enzo.gedima_gination.R;
import com.enzo.gedima_gination.utils.Global;
import com.enzo.gedima_gination.utils.Toast;
import com.enzo.gedima_gination.database.dao.DataDao;
import com.enzo.gedima_gination.database.dao.ParticipationsDao;
import com.enzo.gedima_gination.models.Data;
import com.enzo.gedima_gination.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          MainActivity
 * Description:    Class gérant l'activité principale pour importer les données
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 19/04/2022       Description: Modification de la méthodes onFailure
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class MainActivity extends AppCompatActivity {
    //Variables
    AsyncHttpClient request;
    Toast toast;
    DataDao dataDao;
    ParticipationsDao participationsDao;
    ApiController apiController;
    Data data;
    Context context;

    //Elements d'interface
    Button syncButton;
    TextView principalTextView;
    EditText loginEditText;
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());

        //Définition des valeurs par défaut
        dataDao = new DataDao(this);
        participationsDao = new ParticipationsDao(this);
        context = getApplicationContext();
        toast = new Toast(context);
        request = new AsyncHttpClient();
        setValueEmulator();

        //Définition des Elements d'interface
        syncButton = (Button) findViewById(R.id.syncButton);
        principalTextView = (TextView) findViewById(R.id.principalTextView);
        loginEditText = (EditText) findViewById(R.id.loginEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        //Définition des listeners
        syncButton.setOnClickListener(syncButtonListener);

        checkData();
        loginEditText.setVisibility(View.VISIBLE);
        passwordEditText.setVisibility(View.VISIBLE);
        syncButton.setVisibility(View.VISIBLE);

        //Message de remerciement
        final Intent intent = getIntent();
        String message = intent.getStringExtra("MESSAGE");
        if( message != null){
            toast.toastLong(message);
        }
    }

    //Methods
    /**
     * Methodes utiliser pour definir quelle url est utiliser si l'application est utiliser
     * sur un emulateur ou non
     */
    public void setValueEmulator()
    {
        if(Utils.isEmulator()) {
            apiController = new ApiController(Global.ENDPOINT_LAN, 5000, Global.token);
            Global.url = Global.ENDPOINT_LAN;
        }else{
            apiController = new ApiController(Global.ENDPOINT_WAN, 5000, Global.token);
            Global.url = Global.ENDPOINT_WAN;
        }
    }

    /**
     * Methodes utiliser pour récupérer les données du concours depuis l'api ou la base
     * embarqué si elles y sont deja
     */
    public void saveData(){
        Log.i("DATA","Checking Data");
        apiController.setToken(Global.token);
        if(dataDao.selectionnerData("1") == null) {
            apiController.saveData(dataDao);
        }

        data = dataDao.selectionnerData("1");
    }

    /**
     * Methodes utiliser pour utiliser pour vérifier les participations n'ont pas déja été importer
     */
    public void checkData(){
        apiController.setToken(Global.token);
        if(dataDao.selectionnerData("1") != null) {
            data = dataDao.selectionnerData("1");
            if(data.getSync() == 1) {
                Intent intent = new Intent(MainActivity.this, VotantActivity.class);
                startActivity(intent);
            }
        }
    }

    /**
     * Methodes pour vérifier que la periode de participation est bien terminer que le debut des
     * votes n'a pas commencer
     * @return true si les participations sont fini et que les votes n'ont pas commencer sinon false
     */
    public boolean checkDate(){
        String dateDuJour = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String finParticipation = data.getFinParticipation();
        String debutVote = data.getDebutVote();

        //Valeur de date de test
        //finParticipation = "2022-06-05";
        debutVote = "2022-06-10";

        //Vérification que la periode de participation est bien terminé
        if(!Utils.compareTwoDate(finParticipation, dateDuJour))
        {
            toast.toastShort(context.getString(R.string.participationDate));
            return false;
        }

        //Vérification que la periode de vote est bien terminé
        if(Utils.compareTwoDate(debutVote, dateDuJour))
        {
            toast.toastShort(context.getString(R.string.voteDate));
            return false;
        }

        return true;
    }

    //Listeners
    /**
     * Listener du button permettant d'initialiser la synchronisation des données
     */
    public View.OnClickListener syncButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.i("INFO", Global.url);
            if(loginEditText.getText().toString().equals("") || passwordEditText.getText().toString().equals("")){
                toast.toastShort(context.getString(R.string.loginInfoInvalid));
                return;
            }

            toast.toastShort(context.getString(R.string.connexion));

            try {
                //Json object contenant les identifiant de connexion du gestionnaire
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("username",loginEditText.getText().toString());
                jsonObject.put("password",passwordEditText.getText().toString());
                StringEntity entity = new StringEntity(jsonObject.toString());
                Log.i("TEST", Global.url + " " + loginEditText.getText().toString() + " " + passwordEditText.getText().toString());
                request.post(context, Global.url + "api-token-auth/", entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        try {
                            Log.i("SUCCESS", "Code response = " + statusCode + " " + response.getString("token"));

                            Global.token = response.getString("token");

                            saveData();
                            //Vérification de la présence de l'objet Data
                            if(data == null){
                                principalTextView.setVisibility(View.VISIBLE);
                                principalTextView.setText(R.string.erreurServeur);
                                return;
                            }

                            if(!checkDate()){ return; }

                            //Synchronisation des données
                            toast.toastShort(context.getString(R.string.sync));
                            apiController.saveParticipations(participationsDao);
                            data.SetSync(1);
                            dataDao.modifierData(data);
                            Intent intent = new Intent(MainActivity.this, VotantActivity.class);
                            startActivity(intent);
                        } catch (Exception e) { e.printStackTrace(); }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        toast.toastShort("ERREUR " + statusCode + " Erreur = " + throwable.toString());
                        Log.i("ERREUR", statusCode + " Erreur = " + throwable.toString());
                    }
                });
            } catch (Exception e) { e.printStackTrace(); }
        }
    };
}
