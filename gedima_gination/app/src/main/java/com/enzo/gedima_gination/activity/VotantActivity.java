package com.enzo.gedima_gination.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.enzo.gedima_gination.R;
import com.enzo.gedima_gination.database.dao.DataDao;
import com.enzo.gedima_gination.models.Data;
import com.enzo.gedima_gination.utils.Global;
import com.enzo.gedima_gination.utils.Toast;
import com.enzo.gedima_gination.database.dao.VotantDao;
import com.enzo.gedima_gination.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          VotantActivity
 * Description:    Class gérant l'activité permetant a un votant de s'identifier
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class VotantActivity extends AppCompatActivity {
    //Variables
    Toast toast;
    Context context;
    VotantDao votantDao;
    DataDao dataDao;
    Data data;

    //Elements d'interface
    EditText emailText;
    EditText nomText;
    EditText prenomText;
    EditText codeText;
    CheckBox consentementCheck;
    Button voterButton;
    Button exporterButton;

    /**
     * Désactivation du retour en arrière sauf si l'application est sur un emulateur et que le
     * debug est active
     */
    @Override
    public void onBackPressed() {
        if(Utils.isEmulator() && Global.DEBUG) {
            super.onBackPressed();
            return;
        }
        toast.toastShort(context.getString(R.string.noBack));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_votant);

        //Définition des valeurs par défaut
        context = getApplicationContext();
        votantDao = new VotantDao(this);
        toast = new Toast(context);
        dataDao = new DataDao(context);
        data = dataDao.selectionnerData("1");

        //Définition des Elements d'interface
        emailText = (EditText) findViewById(R.id.emailText);
        nomText = (EditText) findViewById(R.id.nomText);
        prenomText = (EditText) findViewById(R.id.prenomText);
        codeText = (EditText) findViewById(R.id.codeText);
        consentementCheck = (CheckBox) findViewById(R.id.consentementCheck);
        voterButton = (Button) findViewById(R.id.voterButton);
        exporterButton = (Button) findViewById(R.id.exporterButton);

        //Définition des listeners
        voterButton.setOnClickListener(voterButtonListener);
        exporterButton.setOnClickListener(exporterButtonListener);

        //Affiche le message de remerciement
        final Intent intent = getIntent();
        String message = intent.getStringExtra("VOTANT_MESSAGE");
        if( message != null){
            toast.toastLong(message);
        }

        //Cache le bouton d'exportation si la periode de vote n'est pas terminée
        String dateDuJour = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String finVote = data.getFinVote();
        //Valeur de test
        //finVote = "2022-03-15";

        //Gestion de l'affichager des button voter/exporter selon si les votes sont en cours ou non
        if(!Utils.compareTwoDate(finVote, dateDuJour)){
            exporterButton.setVisibility(View.INVISIBLE);
            voterButton.setVisibility(View.VISIBLE);
        }else{
            exporterButton.setVisibility(View.VISIBLE);
            voterButton.setVisibility(View.INVISIBLE);
        }
    }

    //Methods

    /**
     * Methodes permettant de charger l'activité permettant de voter pour les participations
     */
    public void loadChoixActivity() {
        String dateDuJour = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date().getTime());
        Intent intent = new Intent(VotantActivity.this, VoteActivity.class);
        intent.putExtra("VOTANT_CODE", codeText.getText().toString().toUpperCase());
        intent.putExtra("VOTANT_EMAIL", emailText.getText().toString());
        intent.putExtra("VOTANT_NOM", nomText.getText().toString());
        intent.putExtra("VOTANT_PRENOM", prenomText.getText().toString());
        intent.putExtra("VOTANT_CONSENTEMENT", String.valueOf(consentementCheck.isChecked()));
        intent.putExtra("VOTANT_DATE_CONSENTEMENT", dateDuJour);
        startActivity(intent);
    }

    //Listeners
    /**
     * Listener permettant de se connecter affin de voter pour les participations
     */
    public View.OnClickListener voterButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            /* Vérification si l'application est sur un émulateur et que le debug est activé
               Permettant de ne pas a avoir a saisir toute les informations */
            if(Utils.isEmulator() && Global.DEBUG) {
                loadChoixActivity();
            }

            //Patternes utiliser pour les regex du l'email et du code de ticket
            Pattern pEmail = Pattern.compile("^(.+)@(.+)$");
            Pattern pCode = Pattern.compile("(^[A-Z]{3}\\d)\\s([A-Z]{2}\\d{2})\\s(\\d[A-Z]{2}\\d)$");

            String dateDuJour = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String debutVote = data.getDebutVote();
            String finVote = data.getFinVote();

            //Valeur de test
            //debutVote = "2022-02-11";
            //finVote = "2022-02-16";

            //Vérification que la periode de participation est bien terminé
            if(!Utils.compareTwoDate(debutVote, dateDuJour)) {
                toast.toastShort(context.getString(R.string.voteDebut));
                return;
            }

            //Vérification que la periode de vote est bien terminé
            if(Utils.compareTwoDate(finVote, dateDuJour)) {
                toast.toastShort(context.getString(R.string.voteFin));
                return;
            }

            //Vérification de l'email par regex
            if(!pEmail.matcher(emailText.getText()).matches()){
                toast.toastShort(context.getString(R.string.emailInvalide));
                return;
            }

            //Vérification de la validité du code du ticket
            if(!pCode.matcher(codeText.getText().toString().toUpperCase()).matches()){
                toast.toastShort(context.getString(R.string.codeInvalide));
                return;
            }

            //Vérification du consentement
            if(!consentementCheck.isChecked()){
                toast.toastShort(context.getString(R.string.consentementInvalide));
                return;
            }

            //Vérification si le code saisie n'a pas déjà été utiliser
            if(votantDao.selectionnerVotant(codeText.getText().toString()) != null) {
                toast.toastShort(context.getString(R.string.dejaParticiper));
                return;
            }

            //Vérification que le nom est bien saisie
            if(nomText.getText().toString().equals("")) {
                toast.toastShort("MESSAGE NOM INVALIDE");
                return;
            }

            //Vérification que le prenom est bien saisie
            if(prenomText.getText().toString().equals("")) {
                toast.toastShort("MESSAGE PRENOM INVALIDE");
                return;
            }

            loadChoixActivity();
        }
    };

    /**
     * Methods utiliser pour charger l'activiter d'exportation
     */
    public View.OnClickListener exporterButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(VotantActivity.this, ExportationActivity.class);
            startActivity(intent);
        }
    };

}