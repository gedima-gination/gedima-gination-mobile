package com.enzo.gedima_gination.models;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          Data
 * Description:    Object correspondant a une data
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class Data {
    //variables
    int idData;
    String debutParticipation;
    String finParticipation;
    String debutVote;
    String finVote;
    int sync;

    //Constructor
    public Data(){}

    public Data(int idData,String debutParticipation, String finParticipation, String debutVote, String finVote) {
        this.idData = idData;
        this.debutParticipation = debutParticipation;
        this.finParticipation = finParticipation;
        this.debutVote = debutVote;
        this.finVote = finVote;
        this.sync = 0;
    }

    public Data(int idData,String debutParticipation, String finParticipation, String debutVote,
                String finVote, int sync) {
        this.idData = idData;
        this.debutParticipation = debutParticipation;
        this.finParticipation = finParticipation;
        this.debutVote = debutVote;
        this.finVote = finVote;
        this.sync = sync;
    }

    //Getters
    public int getIdData() { return idData; }

    public String getDebutParticipation() {
        return debutParticipation;
    }

    public String getFinParticipation() {
        return finParticipation;
    }

    public String getDebutVote() {
        return debutVote;
    }

    public String getFinVote() {
        return finVote;
    }

    public int getSync() {
        return sync;
    }

    //Setters
    public void setIdData(int idData) { this.idData = idData; }

    public void setDebutParticipation(String debutParticipation) {
        this.debutParticipation = debutParticipation;
    }

    public void setFinParticipation(String finParticipation) {
        this.finParticipation = finParticipation;
    }

    public void setDebutVote(String debutVote) {
        this.debutVote = debutVote;
    }

    public void setFinVote(String finVote) {
        this.finVote = finVote;
    }

    public void SetSync(int sync) { this.sync = sync; }
}
