package com.enzo.gedima_gination.models;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          Participations
 * Description:    Object correspondant a une participation
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class Participations {
    //Variables
    int idParticipation;
    String titre;
    String description;
    String dateCreation;
    int vote;

    //Constructor
    public Participations(){}

    public Participations(int idParticipation, String titre, String description, String dateCreation, int vote) {
        this.idParticipation = idParticipation;
        this.titre = titre;
        this.description = description;
        this.dateCreation = dateCreation;
        this.vote = vote;
    }

    //Getters
    public int getIdParticipation() {
        return idParticipation;
    }

    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public int getVote() { return vote; }

    //Setters
    public void setIdParticipation(int idParticipation) { this.idParticipation = idParticipation; }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateCreation(String dateCreation) { this.dateCreation = dateCreation; }

    public void setVote(int vote) { this.vote = vote; }

    public void addUnVote(int vote) { this.vote += vote; }
}
