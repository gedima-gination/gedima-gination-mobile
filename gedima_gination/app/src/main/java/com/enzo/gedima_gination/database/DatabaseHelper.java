package com.enzo.gedima_gination.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          DatabaseHelper
 * Description:    Class gérant les tables de la base embarque
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class DatabaseHelper  extends SQLiteOpenHelper {
    //Constructor
    public DatabaseHelper(Context context) {
        super(context, "gedimagination.db", null, 1);
    }


    //Methods

    /**
     * Création de la base de donnée
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE participation ("
                + "id_participation INTEGER NOT NULL PRIMARY KEY,"
                + "titre TEXT NOT NULL,"
                + "description TEXT NOT NULL,"
                + "date_creation TEXT NOT NULL,"
                + "vote INTEGER);");

        db.execSQL("CREATE TABLE votant ("
                + "code_votant TEXT NOT NULL PRIMARY KEY,"
                + "email TEXT NOT NULL,"
                + "nom TEXT NOT NULL,"
                + "prenom TEXT NOT NULL,"
                + "consentement TEXT NOT NULL,"
                + "date_consentement TEXT NOT NULL);");

        db.execSQL("CREATE TABLE data ("
                + "id_data INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                + "debut_participation TEXT NOT NULL,"
                + "fin_participation TEXT NOT NULL,"
                + "debut_vote TEXT NOT NULL,"
                + "fin_vote TEXT,"
                + "sync INTEGER);");

        db.execSQL("CREATE TABLE vote ("
                + "code_votant TEXT NOT NULL,"
                + "id_participation INT NOT NULL,"
                + "vote INT NOT NULL,"
                +"FOREIGN KEY (code_votant) REFERENCES votant(code_votant),"
                +"FOREIGN KEY (id_participation) REFERENCES participation(id_participation),"
                + "PRIMARY KEY(code_votant,id_participation));");
    }

    /**
     * Gestion des mises a jour de la base de donnée
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS participation;");
        db.execSQL("DROP TABLE IF EXISTS votant;");
        db.execSQL("DROP TABLE IF EXISTS data;");

        onCreate(db);
    }
}
