package com.enzo.gedima_gination.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.enzo.gedima_gination.database.DatabaseHelper;
import com.enzo.gedima_gination.models.Votant;
import com.enzo.gedima_gination.models.Vote;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          VoteDao
 * Description:    Class gérant les requetes pour la table Vote
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class VoteDao {
    //Variables
    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;

    //Constructor
    public VoteDao(Context context){
        this.databaseHelper = new DatabaseHelper(context);
        this.database = databaseHelper.getWritableDatabase();
    }

    /**
     * Methodes permettant de selectionenr un vote
     * @param codeVotant code du votant que l'on veux récupérer
     * @param idParticipation id de la participation
     * @return un vote correspondant au couple codeVotant et idParticipation ou null si aucun vote
     * n'a ete trouver
     */
    public Vote selectionnerVote(String codeVotant, String idParticipation) {
        Cursor cursor = database.rawQuery("SELECT * FROM vote WHERE code_votant = ? AND "
                        + "id_participation = ?",
                new String[]{codeVotant,idParticipation});
        cursor.moveToFirst();

        Vote vote = null;

        if(cursor.getCount() > 0){
            vote = new Vote(cursor.getString(0),cursor.getInt(1), cursor.getInt(2));
        }

        cursor.close();
        return vote;
    }

    /**
     * Methodes permetant d'ajouter un vote
     * @param vote vote que l'on veux ajouter
     */
    public void ajouterVote(Vote vote){
        ContentValues values = new ContentValues();

        values.put("code_votant", vote.getCodeVotant());
        values.put("id_participation", vote.getIdParticipation());
        values.put("vote", vote.getVote());

        database.insert("vote", null, values);
    }
}
