package com.enzo.gedima_gination.utils;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          Global
 * Description:    Class permettant de gérer toute les variables global accessible partout
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/

public class Global {
    //Constantes
    public static final boolean DEBUG = false;
    public static final String ENDPOINT_WAN = "http://10.0.2.2:9000/";
    public static final String ENDPOINT_LAN = "http://10.0.2.2:9000/";

    //Variables
    public static String token = "";
    public static String url = "";


    //Méthodes
}
