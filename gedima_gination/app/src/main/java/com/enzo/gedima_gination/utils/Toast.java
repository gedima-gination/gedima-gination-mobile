package com.enzo.gedima_gination.utils;

import android.content.Context;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          Toast
 * Description:    Class permetant d'utiliser des toast plus facilement
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class Toast {
    //Variables
    Context context;

    //Constructor
    public Toast(Context context) {
        this.context = context;
    }

    //Methods

    /**
     * Methods utiliser pour afficher un message dans un toast longue
     * @param msg message que l'on veux afficher dans le toast
     */
    public void toastLong(String msg){
        CharSequence text = msg;
        int duration = android.widget.Toast.LENGTH_LONG;
        android.widget.Toast toast = android.widget.Toast.makeText(this.context, text, duration);
        toast.show();
    }

    /**
     * Methods utiliser pour afficher un message dans un toast cours
     * @param msg message que l'on veux afficher dans le toast
     */
    public void toastShort(String msg){
        CharSequence text = msg;
        int duration = android.widget.Toast.LENGTH_SHORT;
        android.widget.Toast toast = android.widget.Toast.makeText(this.context, text, duration);
        toast.show();
    }
}
