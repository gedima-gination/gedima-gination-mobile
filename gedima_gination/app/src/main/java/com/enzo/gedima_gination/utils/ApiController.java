package com.enzo.gedima_gination.utils;

import android.util.Log;
import android.widget.ProgressBar;

import com.enzo.gedima_gination.database.dao.DataDao;
import com.enzo.gedima_gination.database.dao.ParticipationsDao;
import com.enzo.gedima_gination.models.Data;
import com.enzo.gedima_gination.models.Participations;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          ApiController
 * Description:    Class gérant les requete api pour data et participations
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          Controller temporaire doit être remplacer par un Client Async
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class ApiController {
    //Variables
    String endpoint;
    int timeout;
    String token;

    //Constructor
    public ApiController(){}
    public ApiController(String endpoint, int timeout, String token) {
        this.endpoint = endpoint;
        this.timeout = timeout;
        this.token = token;
    }

    //Getters
    public String getEndpoint() { return endpoint; }
    public int getTimeout() { return timeout; }
    public String getToken() { return token; }

    //Setters
    public void setEndpoint(String endpoint) { this.endpoint = endpoint; }
    public void setTimeout(int timeout) { this.timeout = timeout; }
    public void setToken(String token) { this.token = token; }

    //Methods

    /**
     * Méthodes permettant de récupérer les informations sur le concours depuis l'api
     * @param dataDao DAO permettant la gestion des objets Data
     */
    public void saveData(DataDao dataDao)
    {
        StringBuilder result = new StringBuilder();
        String line;
        try {
            URL url = new URL(this.endpoint + "api/data/");
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            httpConnection.setRequestProperty("Accept", "application/json");
            httpConnection.setRequestProperty ("Authorization", "Token " + this.token);
            httpConnection.setUseCaches(false);
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setConnectTimeout(this.timeout);
            httpConnection.setReadTimeout(this.timeout);
            httpConnection.connect();

            InputStream in = new BufferedInputStream(httpConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            while ((line = reader.readLine()) != null) { result.append(line); }
            JSONArray response = new JSONArray(result.toString());
            try {
                JSONObject results = response.getJSONObject(0);
                int idData = results.getInt("id_data");
                String debutParticipation = results.getString("debut_participation");
                String finParticipation = results.getString("fin_participation");
                String debutVote = results.getString("debut_vote");
                String finVote = results.getString("fin_vote");
                Data data = new Data(idData,debutParticipation,finParticipation,debutVote,finVote);
                dataDao.ajouterData(data);
                Log.i("DATA_HTTP", results.toString());
            } catch (JSONException e) {
                Log.e("DATA_HTTP", e.toString());
            }
            Log.i("DATA_HTTP", response.toString());
        } catch (Exception e) {
            Log.e("DATA_HTTP", e.toString());
        }

    }

    /**
     * Méthodes permettant de récupérer les participations de l'api
     * @param participationsDao DAO de l'objet participation
     */
    public void saveParticipations(ParticipationsDao participationsDao)
    {
        StringBuilder result = new StringBuilder();
        String line;
        try {
            URL url = new URL(this.endpoint + "api/participations/");
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            httpConnection.setRequestProperty("Accept", "application/json");
            httpConnection.setRequestProperty ("Authorization", "Token " + this.token);
            httpConnection.setUseCaches(false);
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setConnectTimeout(this.timeout);
            httpConnection.setReadTimeout(this.timeout);
            httpConnection.connect();

            InputStream in = new BufferedInputStream(httpConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            while ((line = reader.readLine()) != null) { result.append(line); }
            JSONArray response = new JSONArray(result.toString());
            try {
                for (int i=0; i < response.length(); i++) {
                    float progress = (float)(i / response.length() * 100.0);
                    Log.i("PROGRESS", String.valueOf(i));
                    Log.i("PROGRESS", String.valueOf(response.length()));
                    Log.i("PROGRESS", String.valueOf(progress));

                    JSONObject json = response.getJSONObject(i);
                    Participations participation = new Participations();

                    participation.setIdParticipation(json.getInt("id_participation"));
                    participation.setTitre(json.getString("titre"));
                    participation.setDescription(json.getString("description"));
                    participation.setDateCreation(json.getString("date_creation"));
                    participation.setVote(json.getInt("vote"));
                    participationsDao.ajouterParticipation(participation);
                }
            } catch (JSONException e) {
                Log.e("PARTICIPATIONS_HTTP", e.toString());
            }
            Log.i("PARTICIPATIONS_HTTP", response.toString());
        } catch (Exception e) {
            Log.e("PARTICIPATIONS_HTTP", e.toString());
        }

    }
}
