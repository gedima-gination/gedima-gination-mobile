package com.enzo.gedima_gination.activity;

import static java.lang.Integer.parseInt;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.enzo.gedima_gination.R;
import com.enzo.gedima_gination.database.dao.VoteDao;
import com.enzo.gedima_gination.models.Vote;
import com.enzo.gedima_gination.utils.Global;
import com.enzo.gedima_gination.utils.Toast;
import com.enzo.gedima_gination.database.dao.ParticipationsDao;
import com.enzo.gedima_gination.database.dao.VotantDao;
import com.enzo.gedima_gination.models.Participations;
import com.enzo.gedima_gination.models.Votant;
import com.enzo.gedima_gination.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          VoteActivity
 * Description:    Class gérant l'activité pour exporter le vote d'un participant
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class VoteActivity extends AppCompatActivity {
    //Variables
    Toast toast;
    Context context;
    VotantDao votantDao;
    VoteDao voteDao;
    ParticipationsDao participationsDao;
    ArrayList<HashMap<String,String>> lesParticipations;
    LayoutInflater layoutInflater;
    View header;
    Votant votant;
    int nbVote = 3;

    //Elements d'interface
    ListView listViewParticipations;
    SimpleAdapter adapter;
    ImageView vote1ImageView;
    ImageView vote2ImageView;
    ImageView vote3ImageView;
    TextView vote1TextView;
    TextView vote2TextView;
    TextView vote3TextView;

    /**
     * Désactivation du retour en arrière
     */
    @Override
    public void onBackPressed() {
        if(Utils.isEmulator() && Global.DEBUG) {
            super.onBackPressed();
            return;
        }
        toast.toastShort(context.getString(R.string.noBack));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);

        //Définition des valeurs par défaut
        context = getApplicationContext();
        votantDao = new VotantDao(this);
        participationsDao = new ParticipationsDao(this);
        voteDao = new VoteDao(this);
        toast = new Toast(context);

        //Définition des Elements d'interface
        listViewParticipations = (ListView)findViewById(R.id.listViewParticipations);
        layoutInflater = getLayoutInflater();
        header = layoutInflater.inflate(R.layout.layout_header_lvparticipation,null,false);
        listViewParticipations.addHeaderView(header,null,false);
        vote1ImageView = (ImageView) findViewById(R.id.vote1ImageView);
        vote2ImageView = (ImageView) findViewById(R.id.vote2ImageView);
        vote3ImageView = (ImageView) findViewById(R.id.vote3ImageView);
        vote1TextView = (TextView) findViewById(R.id.vote1TextView);
        vote2TextView = (TextView) findViewById(R.id.vote2TextView);
        vote3TextView = (TextView) findViewById(R.id.vote3TextView);

        //Récupération des information du votant depuis l'ancienne activité
        final Intent intent = getIntent();
        votant = new Votant(intent.getStringExtra("VOTANT_CODE"),
                intent.getStringExtra("VOTANT_EMAIL"),intent.getStringExtra("VOTANT_NOM"),
                intent.getStringExtra("VOTANT_PRENOM"),
                intent.getStringExtra("VOTANT_CONSENTEMENT"),
                intent.getStringExtra("VOTANT_DATE_CONSENTEMENT"));
        if(votantDao.selectionnerVotant(intent.getStringExtra("VOTANT_CODE")) == null) {
            votantDao.ajouterVotant(votant);
        }

        getParticipations();

        //Définition des listeners
        /**
         * Listener permettant de savoir quand un votant clique sur une participation
         */
        listViewParticipations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                HashMap<String, String> item = (HashMap<String, String>) listViewParticipations.getItemAtPosition(position);
                AlertDialog.Builder alert = new AlertDialog.Builder(VoteActivity.this);

                //Informations de l'alert
                alert.setTitle(getString(R.string.voterTitre) + " " + item.get("IDPARTICIPATION"));
                alert.setMessage(R.string.voterMessage);

                //Création d'un EditText affin de récuperer la note
                final EditText input = new EditText(VoteActivity.this);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                alert.setView(input);

                //Création d'une alert affin de connaitre le nombre de point donné
                alert.setPositiveButton(R.string.voter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //Vérification que la note donné est bien comprise entre 1 et 5
                        int value = parseInt("0" + input.getText().toString());
                        if(value > 5 || value <= 0)
                        {
                            toast.toastShort(context.getString(R.string.voterMessage));
                            return;
                        }

                        --nbVote;

                        //Gestion de l'affichage du nombre de vote restant
                        switch (nbVote) {
                            case 2:
                                vote1ImageView.setVisibility(View.INVISIBLE);
                                vote1TextView.setText(getString(R.string.voteText) + " " + item.get("IDPARTICIPATION"));
                                break;

                            case 1:
                                vote2ImageView.setVisibility(View.INVISIBLE);
                                vote2TextView.setText(getString(R.string.voteText) + " " + item.get("IDPARTICIPATION"));
                                break;

                            case 0:
                                vote3ImageView.setVisibility(View.INVISIBLE);
                                vote3TextView.setText(getString(R.string.voteText) + " " + item.get("IDPARTICIPATION"));
                                break;

                            default:
                                break;
                        }

                        //Ajout des points donner par le votant
                        Participations participation = participationsDao.selectionnerParticipation(item.get("IDPARTICIPATION"));
                        participation.addUnVote(value);
                        participationsDao.modifierParticipation(participation);

                        //Sauvegarde du vote en base de donnée embarqué
                        Vote v = new Vote(votant.getCodeVotant(), parseInt(item.get("IDPARTICIPATION")), value);
                        voteDao.ajouterVote(v);

                        //Supprime la participation pour laquelle la note a été attribuer
                        lesParticipations.remove(position-1);
                        adapter.notifyDataSetChanged();

                        // Retout a l'activité de connexion une fois tout les vote réaliser parle votant
                        if(nbVote == 0) loadVotantActivity();
                    }
                });

                alert.setNegativeButton(R.string.annuler, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) { }
                });

                alert.show();
                lesParticipations.remove(this);
                adapter.notifyDataSetChanged();
            }
        });
    }

    //Methods
    /**
     * Methodes permettant de récupérer les informations de participations
     */
    @SuppressLint("Range")
    public void getParticipations()
    {
        //Récupération des information depuis la base de donnée
        Cursor cursor = participationsDao.selectionnerTouteLesParticipations();
        lesParticipations = new ArrayList<HashMap<String, String>>();

        //parcours des informations récupérer depuis la base
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            int idParticipation = cursor.getInt(cursor.getColumnIndex("id_participation"));
            String titre = cursor.getString(cursor.getColumnIndex("titre"));
            String description = cursor.getString(cursor.getColumnIndex("description"));
            String dateCreation = cursor.getString(cursor.getColumnIndex("date_creation"));
            int vote = cursor.getInt(cursor.getColumnIndex("vote"));

            HashMap<String,String> uneParticipation = new HashMap<String, String>();
            uneParticipation.put("IDPARTICIPATION", String.valueOf(idParticipation));
            uneParticipation.put("TITRE", titre);
            uneParticipation.put("DESCRIPTION", description);
            uneParticipation.put("DATECREATION", dateCreation);
            uneParticipation.put("VOTE", String.valueOf(vote));
            lesParticipations.add(uneParticipation);
        }

        cursor.close();
        //Ajout des informations dans la listeview pour les afficher
        adapter = new SimpleAdapter(
                this,
                lesParticipations,
                R.layout.layout_ligne_lvparticipation,
                new String[] {"IDPARTICIPATION", "TITRE", "DESCRIPTION", "DATECREATION"},
                new int[] {R.id.colId, R.id.colTitre, R.id.colDescription, R.id.colDateCreation});
        listViewParticipations.setAdapter(adapter);
    }

    /**
     * Methodes permettant de revenir sur la activité de connection après avoir voté
     */
    public void loadVotantActivity() {
        Intent intent = new Intent(VoteActivity.this, VotantActivity.class);
        intent.putExtra("VOTANT_MESSAGE", context.getString(R.string.merciMessage));
        startActivity(intent);
    }
}