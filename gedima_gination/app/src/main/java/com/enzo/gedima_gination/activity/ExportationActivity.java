package com.enzo.gedima_gination.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.enzo.gedima_gination.R;
import com.enzo.gedima_gination.database.dao.DataDao;
import com.enzo.gedima_gination.database.dao.ParticipationsDao;
import com.enzo.gedima_gination.models.Data;
import com.enzo.gedima_gination.utils.ApiController;
import com.enzo.gedima_gination.utils.Global;
import com.enzo.gedima_gination.utils.Toast;
import com.enzo.gedima_gination.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.*;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          ExportationActivity
 * Description:    Class gérant l'activité pour exporter les données
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 31/03/2022       Description: Modification de la requete patch
 * Name: Enzo.L          Date: 19/04/2022       Description: Modification de la méthodes onFailure
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class ExportationActivity extends AppCompatActivity {
    //Variables
    AsyncHttpClient request;
    Toast toast;
    ParticipationsDao participationsDao;
    Context context;
    JSONArray jsonParticipation = new JSONArray();

    //Elements d'interface
    TextView exportationTextView;
    Button exporterButton;
    EditText loginEditText;
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exportation);

        //Définition des valeurs par défaut
        participationsDao = new ParticipationsDao(this);
        context = getApplicationContext();
        toast = new Toast(context);
        request = new AsyncHttpClient();

        //Définition des Elements d'interface
        exportationTextView = (TextView) findViewById(R.id.exportationText);
        exporterButton = (Button) findViewById(R.id.exporterButton);
        loginEditText = (EditText) findViewById(R.id.loginEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        //Définition des listeners
        exporterButton.setOnClickListener(exporterButtonListener);

        getParticipations();
    }

    //Methods

    /**
     * Fonction permettant de récupérer les informations de participations
     */
    @SuppressLint("Range")
    public void getParticipations()
    {
        //Récupération des information depuis la base de donnée
        Cursor cursor = participationsDao.selectionnerTouteLesParticipations();

        //parcours des informations récupérer depuis la base
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            JSONObject jsonObj = new JSONObject();
            int idParticipation = cursor.getInt(cursor.getColumnIndex("id_participation"));
            String titre = cursor.getString(cursor.getColumnIndex("titre"));
            String description = cursor.getString(cursor.getColumnIndex("description"));
            String dateCreation = cursor.getString(cursor.getColumnIndex("date_creation"));
            int vote = cursor.getInt(cursor.getColumnIndex("vote"));
            try {
                jsonObj.put("id_participation", String.valueOf(idParticipation));
                jsonObj.put("titre", titre);
                jsonObj.put("description", description);
                jsonObj.put("date_creation", dateCreation);
                jsonObj.put("vote", String.valueOf(vote));

                jsonParticipation.put(jsonObj);
            } catch (JSONException e) {
                Log.e("ERROR",e.getMessage());
            }
        }
        cursor.close();

    }


    //Listeners
    public View.OnClickListener exporterButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.i("INFO", Global.url);
            Log.i("INFO", loginEditText.getText().toString());
            Log.i("INFO", passwordEditText.getText().toString());
            Log.i("INFO", Global.token);
            if(loginEditText.getText().toString().equals("") || passwordEditText.getText().toString().equals("")){
                toast.toastShort(context.getString(R.string.loginInfoInvalid));
                return;
            }
            exportationTextView.setVisibility(View.VISIBLE);
            exportationTextView.setText(context.getString(R.string.exportationEncours));
            try {
                //Json object contenant les identifiant de connexion du gestionnaire
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("username",loginEditText.getText().toString());
                jsonObject.put("password",passwordEditText.getText().toString());
                StringEntity entity = new StringEntity(jsonObject.toString());

                request.post(context, Global.url + "api-token-auth/", entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        try {
                            Log.i("SUCCESS", "Code response = " + statusCode + " " + response.getString("token"));
                            Log.i("SUCCESS","test de julien");
                            Log.i("INFO", Global.token);
                            Log.i("INFO", response.getString("token"));

                            Global.token = response.getString("token");
                            String auth = "Token " + Global.token;
                            request.addHeader("Authorization", auth);
                            for (int i = 0; i < jsonParticipation.length() ; i++){
                                JSONObject obj = jsonParticipation.getJSONObject(i);
                                StringEntity entity = new StringEntity(obj.toString());
                                String url = Global.url + "api/participations/" + obj.getString("id_participation") + "/";
                                Log.i("EXPORTATION", url);

                                request.patch(context, url, entity, "application/json", new JsonHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                        super.onSuccess(statusCode, headers, response);
                                        Log.i("SUCCESS", "Code response = " + statusCode);
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                        super.onFailure(statusCode, headers, throwable, errorResponse);
                                        toast.toastShort("ERREUR " + statusCode + " Erreur = " + throwable.toString());
                                        Log.i("ERREUR", statusCode + " Erreur = " + throwable.toString());
                                        exportationTextView.setVisibility(View.INVISIBLE);
                                        exporterButton.setVisibility(View.VISIBLE);
                                        loginEditText.setVisibility(View.VISIBLE);
                                        passwordEditText.setVisibility(View.VISIBLE);
                                    }
                                });
                            }

                            exportationTextView.setVisibility(View.VISIBLE);
                            exportationTextView.setText(context.getString(R.string.exportationFini));
                            exporterButton.setVisibility(View.INVISIBLE);
                            loginEditText.setVisibility(View.INVISIBLE);
                            passwordEditText.setVisibility(View.INVISIBLE);
                        } catch (Exception e) { Log.e("ERREUR", e.getMessage()); }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        toast.toastShort("ERREUR " + statusCode + " Erreur = " + throwable.toString());
                        Log.i("ERREUR", statusCode + " Erreur = " + throwable.toString());
                        exportationTextView.setVisibility(View.INVISIBLE);
                        exporterButton.setVisibility(View.VISIBLE);
                        loginEditText.setVisibility(View.VISIBLE);
                        passwordEditText.setVisibility(View.VISIBLE);
                    }
                });
            } catch (Exception e) { Log.e("ERREUR", e.getMessage());}
        }
    };
}