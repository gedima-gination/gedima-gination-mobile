package com.enzo.gedima_gination.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.enzo.gedima_gination.database.DatabaseHelper;
import com.enzo.gedima_gination.models.Data;
import com.enzo.gedima_gination.models.Participations;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          ParticipationsDao
 * Description:    Class gérant les requetes pour la table Participations
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class ParticipationsDao {
    //Variables
    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;

    //Constructor
    public ParticipationsDao(Context context){
        this.databaseHelper = new DatabaseHelper(context);
        this.database = databaseHelper.getWritableDatabase();
    }

    /**
     * Methodes permettant de selection tout les participation
     * @return un curseur contenant toutes les participations
     */
    public Cursor selectionnerTouteLesParticipations() {
        Cursor cursor = database.rawQuery("SELECT * from participation" , new String[] {});

        if(cursor.getCount() > 0){
            return cursor;
        }

        return null;
    }

    /**
     * Methodes permettant de selection une participation selon son id
     * @param id id de la participation voulu
     * @return la participation si elle a été trouvé sinon null
     */
    public Participations selectionnerParticipation(String id) {
        Cursor cursor = database.rawQuery("SELECT * FROM participation WHERE id_participation = ?",
                new String[]{id});
        cursor.moveToFirst();

        Participations participation = null;

        if(cursor.getCount() > 0){
            participation = new Participations(cursor.getInt(0), cursor.getString(1), cursor.getString(2
            ), cursor.getString(3), cursor.getInt(4));
        }

        cursor.close();
        return participation;
    }

    /**
     * Methodes permettant d'ajouter une nouvelle participations
     * @param participation participation que l'on veux ajouter
     */
    public void ajouterParticipation(Participations participation){
        //Valeurs que l'on veut ajouter
        ContentValues values = new ContentValues();
        values.put("id_participation", participation.getIdParticipation());
        values.put("titre", participation.getTitre());
        values.put("description", participation.getDescription());
        values.put("date_creation", participation.getDateCreation());
        values.put("vote", participation.getVote());

        //Requete vers la base de données
        database.insert("participation", null, values);
    }

    /**
     * Methodes permettant de modifier une participation existante
     * @param participation participation contenant les informations modifier
     */
    public void modifierParticipation(Participations participation){
        //Nouvelle valeurs que l'on veux ajouter
        ContentValues values = new ContentValues();
        values.put("id_participation", participation.getIdParticipation());
        values.put("titre", participation.getTitre());
        values.put("description", participation.getDescription());
        values.put("date_creation", participation.getDateCreation());
        values.put("vote", participation.getVote());

        //Requete vers la base de données
        database.update("participation", values,"id_participation = ?", new String[]{String.valueOf(
                participation.getIdParticipation())});
    }
}
