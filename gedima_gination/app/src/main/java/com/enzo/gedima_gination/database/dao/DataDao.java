package com.enzo.gedima_gination.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.enzo.gedima_gination.database.DatabaseHelper;
import com.enzo.gedima_gination.models.Data;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          DataDao
 * Description:    Class gérant les requetes pour la table Data
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class DataDao {
    //Variables
    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;

    //Constructor
    public DataDao(Context context){
        this.databaseHelper = new DatabaseHelper(context);
        this.database = databaseHelper.getWritableDatabase();
    }

    /**
     * Methodes permettant de selectionner une data avec un id
     * @param id id de la data voulu
     * @return la data si elle a été trouver sinon null
     */
    public Data selectionnerData(String id) {
        Cursor cursor = database.rawQuery("SELECT * FROM data WHERE id_data = ?",
                new String[]{id});
        cursor.moveToFirst();

        Data uneData = null;

        if(cursor.getCount() > 0){
            uneData = new Data(cursor.getInt(0), cursor.getString(1),cursor.getString(2),
                    cursor.getString(3), cursor.getString(4),cursor.getInt(5));
        }

        cursor.close();
        return uneData;
    }

    /**
     * Methodes permettant d'ajouter une data
     * @param data data a ajouter dans la base
     */
    public void ajouterData(Data data){
        //Valeurs que l'on veut ajouter
        ContentValues values = new ContentValues();
        values.put("id_data", data.getIdData());
        values.put("debut_participation", data.getDebutParticipation());
        values.put("fin_participation", data.getFinParticipation());
        values.put("debut_vote", data.getDebutVote());
        values.put("fin_vote", data.getFinVote());
        values.put("sync", data.getSync());

        //Requete vers la base
        database.insert("data", null, values);
    }

    /**
     * Methodes permettant de modifier une data
     * @param data data que l'on veux modifier
     */
    public void modifierData(Data data){
        //Valeurs a modifier
        ContentValues values = new ContentValues();
        values.put("id_data", data.getIdData());
        values.put("debut_participation", data.getDebutParticipation());
        values.put("fin_participation", data.getFinParticipation());
        values.put("debut_vote", data.getDebutVote());
        values.put("fin_vote", data.getFinVote());
        values.put("sync", data.getSync());

        //Requete vers la base
        database.update("data", values,"id_data = ?", new String[]{String.valueOf(
                data.getIdData())});
    }
}
