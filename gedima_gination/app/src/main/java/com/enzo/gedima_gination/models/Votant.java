package com.enzo.gedima_gination.models;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          Votant
 * Description:    Object correspondant a un votant
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class Votant {
    //Variables
    String codeVotant;
    String email;
    String nom;
    String prenom;
    String consentement;
    String dateConsentement;

    //Constructor
    public Votant() {}

    public Votant(String codeVotant, String email, String nom, String prenom, String consentement, String dateConsentement) {
        this.codeVotant = codeVotant;
        this.email = email;
        this.nom = nom;
        this.prenom = prenom;
        this.consentement = consentement;
        this.dateConsentement = dateConsentement;
    }

    //Getters

    public String getCodeVotant() {
        return codeVotant;
    }

    public String getEmail() {
        return email;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getConsentement() {
        return String.valueOf(consentement);
    }

    public String getDateConsentement() { return dateConsentement; }
    //Setters

    public void setCodeVotant(String codeVotant) {
        this.codeVotant = codeVotant;
    }

    public void setEmail(String email) { this.email = email; }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setConsentement(String consentement) {
        this.consentement = consentement;
    }

    public void setDateConsentement(String dateConsentement) { this.dateConsentement = dateConsentement; }
}
