package com.enzo.gedima_gination.utils;

import android.os.Build;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/* ---------------------------------------
 * Namespace:      <Namespace>
 * Class:          Utils
 * Description:    Regroupe différentes fonctions utiles
 * Auteur:         Enzo.L                    Date: <Date>
 * Notes:          <Notes>
 * Historique de révision:
 * Name: Enzo.L          Date: 29/03/2022       Description: Ajout des commentaires
 * Name: Enzo.L          Date: 28/05/2022       Description: Vérification final correction diverse
 * -------------------------------------*/
public class Utils {

    /**
     * Méthodes permetant de savoir si l'application est lancé depuis un emulateur ou depuis un vrai appareil
     * @return true si l'application est lancé depuis un emulateur, false sinon
     */
    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    /**
     * Méthodes permettant de savoir si la date A est avant la date B
     * @param dateAString Date A en format String (2022-05-28)
     * @param dateBString Date B en format String (2022-05-28)
     * @return true si la date A est avant la date B, false sinon
     */
    public static boolean compareTwoDate(String dateAString,  String dateBString){
        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd");
        Date dateA = null;
        Date dateB = null;
        try {
            dateA = dateFormat.parse(dateAString);
            dateB = dateFormat.parse(dateBString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateA.before(dateB);
    }
}
